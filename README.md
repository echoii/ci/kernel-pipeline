# Kernel Pipeline

Common GitLab CI job definitions for Ubuntu kernel development.

## Table of contents
* [Basic Use](#basic-use)
* [Jobs in Depth](#jobs-in-depth)
* [Support](#support)

## Basic Use

To use Kernel Pipeline job definitions, the simplest way is to include one of the series-based yml file, e.g. `bionic.yml`:
```yaml
include:
  - https://gitlab.com/echoii/ci/kernel-pipeline/raw/master/bionic.yml
```
This will create a pipeline of two stages, build and aptly, which firstly build the binary package from your kernel source repository, and then create a apt repository from built artifacts.

If you want to customize a few settings for the default setup, add a `variables` section to the target job:
```yaml
include:
  - https://gitlab.com/echoii/ci/kernel-pipeline/raw/master/bionic.yml

build:
  variables:
    JOB_TARGETS: "clean binary-headers"
```

Or, you may want to include the templates only and restart real job definition by your self:

```yaml
include:
  - https://gitlab.com/echoii/ci/kernel-pipeline/raw/master/definitions.yml

build-generic:
  extends: .build-template
  variables:
    JOB_SUITE: "bionic"
    JOB_TARGETS: "clean binary-generic"

build-lowlatency:
  extends: .build-template
  variables:
    JOB_SUITE: "bionic"
    JOB_TARGETS: "clean binary-lowlatency"
```

## Jobs in Depth
### Build
The default build job definition supports compiling `-generic`, `-oem`, `-oem-osp1` from Ubuntu kernel source package repositories, and mainline kernel source.

Recognized job parameters are:
* **JOB_IMAGES**:
* **JOB_KTEAM_TOOLS_URL**:
* **JOB_KTEAM_TOOLS_BRANCH**:
* **JOB_KTEAM_TOOLS_PATCH_BRANCH**:
* **JOB_SUITE**:
* **JOB_ARCH**:
* **JOB_PARAMS**:
* **JOB_TARGETS**:
### Aptly
The [Aptly](https://www.aptly.info/) task runs in the publish stage and will save published apt repository files as its artifacts, so downstream CI tasks may access built binary/source packages directly through artifacts url via apt.

To find the url manually, open the apt build job page, click `Browse` (in the `Job artifacts` pane) -> `output` -> `aptly` -> `index.html`. In brief it takes:

```bash
$ export BASE_URL=${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/raw/${JOB_REPO_PATH}
$ sudo wget ${BASE_URL}/public-key.asc | sudo apt-key add -
$ echo | sudo tee /etc/apt/sources.list.d/job-${CI_JOB_ID}.list <<EOF
deb ${BASE_URL} ${JOB_DISTRIBUTION} ${JOB_COMPONENT}
# deb-src ${BASE_URL} ${JOB_DISTRIBUTION} ${JOB_COMPONENT}
EOF
$ sudo apt-get update
```

To specify repository signing key, export the gpg key/passphrase as CI / CD [Variables](https://gitlab.com/help/ci/variables/README#variables) `ECHOII_APTLY_GPG_KEY` and `ECHOII_APTLY_GPG_PASSPHASE`. Otherwise, an automatically generated one will be used.

Recognized job parameters are:
* **JOB_DISTRIBUTION**:
* **JOB_COMPONENT**:
* **JOB_REPO_PATH**:
* **JOB_PUBKEY_FILENAME**:
## Support
Just file an issue on the [Issues](https://gitlab.com/echoii/ci/kernel-pipeline/issues) page.